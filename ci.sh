#!/usr/bin/env bash
set -euo pipefail

source ./scripts/shared/_log_helper.sh
fn_set_default_log_section_fn

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export BASEDIR

if ! command -v docker ; then
    echo 'Docker is missing!' >&2
    exit 1
fi
cd "${BASEDIR}"

# Quick validation of non-existent tasks
if [[ -z "${1:-}" ]]; then
  echo "ERROR: Task name not specified!" >&2
  ./scripts/help.sh
  exit 1
elif [[ ! -f "./scripts/${1}.sh" ]]; then
  echo "ERROR: Task '${1}' doesn't exist!" >&2
  ./scripts/help.sh
  exit 1
fi

# Set global constants
set -o allexport
# shellcheck disable=SC1091
source ./scripts/shared/_constants.sh
set +o allexport

# Help and clean should be quick (and not need the shared functions or settings)
if [[ "${1}" == 'help' || "${1}" == 'clean' ]]; then
  "./scripts/${1}.sh"
  exit 0
fi

# Bring shared functions into scope for all sub-scripts
set -o allexport
# shellcheck disable=SC1091
source ./scripts/shared/_log_helper.sh
# shellcheck disable=SC1091
source ./scripts/shared/_check_settings.sh
# shellcheck disable=SC1091
source ./scripts/shared/_version_helper.sh

# Bring config settings into scope for all sub-scripts
settings_file="${CONFIG_FILE:-${CONST_FALLBACK_CONFIG_FILE}}"
if [[ -f "${settings_file}" ]]; then
  echo "Loading settings from: '${settings_file}'"
  # shellcheck disable=SC1090
  source "${settings_file}"
else
  echo "WARNING: A settings file to load was not detected. The path to a file can be supplied in CONFIG_FILE or a file can be placed at '${CONST_FALLBACK_CONFIG_FILE}'" >&2
fi
set +o allexport

if [[ -z "${WORK_DIR:-}" ]];
then
    WORK_DIR=$(pwd)
    export WORK_DIR
fi

if [[ -z "${GIT_COMMIT_SHA:-}" ]];
then
    GIT_COMMIT_SHA=$(git rev-parse HEAD)
    export GIT_COMMIT_SHA
fi

"./scripts/${1}.sh"
