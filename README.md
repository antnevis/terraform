# terraform Docker Container

Docker Hub [antnevis/terraform](https://hub.docker.com/repository/docker/antnevis/terraform)

This repository builds upon [hashicorp/terraform](https://hub.docker.com/r/hashicorp/terraform)
docker and adds the below list of extra packages:
* curl
* bash
* jq
* python3
* py3-pip

Please see [hashicorp/terraform](https://hub.docker.com/r/hashicorp/terraform) for usage
