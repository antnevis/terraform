# shellcheck shell=bash

fn_build_version() {
  _fn_check_tf_version_settings
  BUILD_VERSION=${TERRAFORM_VERSION}
  export BUILD_VERSION
}
