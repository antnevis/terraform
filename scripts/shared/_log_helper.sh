# shellcheck shell=bash

# Function to log a section start
# Usage: fn_section_start <section name>
fn_default_section_start() {
  echo "Start ${1}"
}
export -f fn_default_section_start

# Function to log a section end
# Usage: fn_section_end <section name>
fn_default_section_end() {
  echo "End ${1}"
}
export -f fn_default_section_end

fn_set_default_log_section_fn() {
  if [[ -z "${BLOCK_START_FN:-}" ]];
  then
    echo "Setting default section start function..."
    export BLOCK_START_FN=fn_default_section_start
  fi
  if [[ -z "${BLOCK_END_FN:-}" ]];
  then
    echo "Setting default section end function..."
    export BLOCK_END_FN=fn_default_section_end
  fi
}
