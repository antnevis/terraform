# shellcheck shell=bash

_fn_check_tf_version_settings() {
  _fn_check_settings "TERRAFORM_VERSION"
}

fn_check_auth_settings() {
  local settings=("DOCKER_ACCESS_TOKEN" \
                  "DOCKER_USER" )
  _fn_check_settings "${settings[@]}"
}

_fn_check_settings() {
  local settings=("$@")
  local has_input_errors="false"

  for var in "${settings[@]}"; do
    if [[ -z "${!var:-}" ]]; then
      echo "ERROR: Environment variable '${var}' must be set in the calling environment" >&2
      has_input_errors="true"
    fi
  done

  # Batch all the errors together to maximise feedback to user
  if [[ "${has_input_errors}" == "true" ]]; then
    exit 1
  fi
}

