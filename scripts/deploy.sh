#!/usr/bin/env bash
# This task compiles the application code, validates the Terraform code, and creates a deployable artifact from them. Does not require credentials or any other pre-requisites
set -euo pipefail

fn_build_version
fn_check_auth_settings

echo "${BUILD_VERSION}"
docker login -p "${DOCKER_ACCESS_TOKEN}" -u "${DOCKER_USER}"

docker push "${BUILDER_TAG_PREFIX}:latest"
docker push "${BUILDER_TAG_PREFIX}:${BUILD_VERSION}"
docker push "${BUILDER_TAG_PREFIX}:${GIT_COMMIT_SHA}"
