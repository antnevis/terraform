#!/usr/bin/env bash
# This task compiles the application code, validates the Terraform code, and creates a deployable artifact from them. Does not require credentials or any other pre-requisites
set -euo pipefail

fn_build_version
echo "${BUILD_VERSION}"

docker build \
  -t "${BUILDER_TAG_PREFIX}:latest" \
  -t "${BUILDER_TAG_PREFIX}:${BUILD_VERSION}" \
  -t "${BUILDER_TAG_PREFIX}:${GIT_COMMIT_SHA}" \
  --target=terraform \
  .
