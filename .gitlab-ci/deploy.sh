#!/usr/bin/env bash
set -euo pipefail

./.gitlab-ci/build.sh

"${BLOCK_START_FN}" "Deploy"
./ci.sh deploy
"${BLOCK_END_FN}" "Deploy"
