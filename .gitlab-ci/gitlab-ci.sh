#!/usr/bin/env bash
set -euo pipefail

if [[ -n "${CI_COMMIT_SHA:-}" ]];
then
    export GIT_COMMIT_SHA="${CI_COMMIT_SHA}"
fi

export DELIMITER_CODE="\r\e[0K"
# Function to log a section start
# Usage: fn_gitlab_section_start <section name>
fn_gitlab_section_start() {
  SECTION_NAME="${1,,}"
  echo -e "section_start:$(date +%s):${SECTION_NAME// /_}${DELIMITER_CODE}${1}"
}
export -f fn_gitlab_section_start

# Function to log a section end
# Usage: fn_gitlab_section_end <section name>
fn_gitlab_section_end() {
  SECTION_NAME=$1
  echo -e "section_end:$(date +%s):${SECTION_NAME// /_}${DELIMITER_CODE}"
}
export -f fn_gitlab_section_end

export BLOCK_START_FN=fn_gitlab_section_start
export BLOCK_END_FN=fn_gitlab_section_end

"./.gitlab-ci/${1}.sh"
