#!/usr/bin/env bash
set -euo pipefail

"${BLOCK_START_FN}" "Build"
./ci.sh build
"${BLOCK_END_FN}" "Build"
