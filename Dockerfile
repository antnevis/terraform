FROM hashicorp/terraform:1.8.1 AS terraform

RUN apk upgrade \
    && apk add rsyslog \
    && chmod 600 /etc/shadow \
    && apk add --update --no-cache \
        curl \
        bash \
        jq \
        python3 \
        py3-pip
